#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "$SSH_KEY" > key.pem
chmod 400 key.pem

if [ "$1" == "BUILD" ];then
echo '[*] Building Program To Docker Images'
echo "[*] Tag $WAKTU"
docker build -t menandap/kubernetsdevops:$CI_COMMIT_BRANCH .
docker login --username=$DOCKER_USER --password=$DOCKER_PASS
docker push menandap/kubernetsdevops:$CI_COMMIT_BRANCH
echo $CI_PIPELINE_ID

elif [ "$1" == "DEPLOY" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P ""
&& cat ~/.ssh/id_rsa.pub
echo '[*] Execute Remote SSH'
# ssh -i key.pem -o "StrictHostKeyChecking no" root@128.199.142.219 "sudo
microk8s.kubectl delete -f service-nginx-nodeport.yaml"
ssh -i key.pem -o "StrictHostKeyChecking no" root@128.199.142.219 "sudo
microk8s.kubectl create -f service-nginx-nodeport.yaml"
echo $CI_PIPELINE_ID
f
